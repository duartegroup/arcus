# ORCA submission for Arcus

Log on to arcus and in your home directory (*/home/username*) run the following
```
mkdir -p repos && cd repos/ && git clone https://bitbucket.org/duartegroup/arcus.git && cd && cp repos/arcus/qorca.py bin/qorca && chmod +x bin/qorca
```
### Python
Arcus only has python 2 installed. To use `qorca` you will need python 3 and numpy. In your $HOME directory run
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh && bash Miniconda3-latest-Linux-x86_64.sh 
```
To install numpy run 
```
conda install -c anaconda numpy
```


### Using qorca

For help run:

```
qorca --help
```
You can submit multiple files simultaneously by either listing the files, e.g.

```
qorca test1.inp test2.inp test3.inp
```

or by using a wildcard

```
qorca *.inp
```

which will submit all the files in the current directory with a .inp extension.


To test your calculation and catch any early errors run

```
qorca filename -test    
```

This is recommended for particularly long calculations. It runs the calculation in the devel partition for ten minutes and can be used to catch any user input errors.