#!/usr/bin/env python3
import os
import argparse
import subprocess

def arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-rf", action="store_true", default=False, help="Run failed orca calculations")
    return parser.parse_args()


def completion(files_in_directory):
    output = [file for file in files_in_directory if file.endswith(".out") and not file.startswith("slurm")]

    success = []
    fail = []
    for out in output:
        with open(out, "r") as orca:
            if "****ORCA TERMINATED NORMALLY****" in orca.read():
                success.append(out)
            else:
                fail.append(out)
    return [success, fail]


def fail_batch(completion_output):
    fail_out = completion_output[1]
    fail_sh = [elem.replace(".out", ".sh") for elem in fail_out]
    return fail_sh


if __name__ == "__main__":
    files_in_directory = os.listdir()
    all_output = completion(files_in_directory)
    args = arguments()

    if args.rf:
        to_run = fail_batch(all_output)
        for file in to_run:
            subprocess.check_call(["sbatch", file])
    else:
        print("\n\nTHESE CALCULATIONS RAN SUCCESSFULLY:")
        for elem in all_output[0]:
            print(elem)
        print("\n\nTHESE CALCULATIONS FAILED:")
        for elem in all_output[1]:
            print(elem)
