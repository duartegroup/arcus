#!/usr/bin/env python3
import subprocess
import argparse
import socket
import sys


def get_args():

    parser = argparse.ArgumentParser(prog="qorca")
    parser.add_argument("filenames", action='store', help='.inp file(s) to submit to the queue', nargs='+')
    parser.add_argument("-notrashtmp", action='store_true', default=False,
                        help="Don't trash the temporary files that may be generated")
    parser.add_argument("-noslurm", action="store_true", default=False,
                        help="Delete the slurm output file generated")
    parser.add_argument("-np", type=int, default=0, help="Override the number of cores specified in the input file")
    parser.add_argument("-nodes", type=int, default=1, help="Change the number of nodes required")
    parser.add_argument("-t", type=int, default=1, help="Calculation runtime. Default = 1hr")
    parser.add_argument("-devel", action="store_true", default=False,
                        help="Run a quick test on devel partition to catch early errors")
    parser.add_argument("-mem1T5", action="store_true", default=False,
                        help="Run the calculation on the mem1T5 partition")
    parser.add_argument("-mem6T", action="store_true", default=False,
                        help="Run the calculation in the mem6T partition")
    parser.add_argument("-gbw", action="store_true", default=False,
                        help="Don't trash the .gbw file")

    return parser.parse_args()


def check_host():
    host = socket.gethostname()
    return True if "htc" in host.split("-") else False


def print_bash_script(inp_filename, htc, notrashtmp, gbw, noslurm, np, nodes, run_time, devel, mem1T5, mem6T):

    sh_filename = str(inp_filename.replace('.inp', '.sh'))
    output = str(inp_filename.replace(".inp", ".out"))

    keyword_line = ''
    with open(inp_filename, 'r') as inp_file:
        for line in inp_file:
            if line.startswith('!'):
                keyword_line = line

    cores = 1
    for item in keyword_line.split():
        if item.startswith("PAL"):
            cores = item[3:]
    if np != 0:
        cores = np

    if htc:
        if int(nodes) > 1:
            sys.exit("\nArcus-htc is only for single node jobs\nUse Arcus-B for multi-node jobs\n")

    with open(sh_filename, 'w') as bash_script:
        bash_script.write("#!/bin/bash\n")
        bash_script.write("\n")
        bash_script.write("#SBATCH --nodes=" + str(nodes) + "\n")
        if devel:
            bash_script.write("#SBATCH --time=0:10:00\n")
        else:
            bash_script.write("#SBATCH --time=" + str(run_time) + ":00:00" + "\n")
        bash_script.write("#SBATCH --job-name=" + inp_filename.replace(".inp", "") + "\n")
        if not htc:
            if mem1T5:
                bash_script.write("#SBATCH --partition=mem1T5\n")
            if mem6T:
                bash_script.write("#SBATCH --partition=mem6T\n")
        if devel:
            if htc:
                bash_script.write("#SBATCH --partition=htc-devel\n")
            else:
                bash_script.write("#SBATCH --partition=devel\n")
        if htc:
            bash_script.write("#SBATCH --ntasks-per-node=" + str(cores) + "\n")
            bash_script.write("\n")
            bash_script.write("module load orca/4.2.0-htc\n")
            bash_script.write("\n")
            bash_script.write("/system/software/generic/orca/orca_4_2_0_linux_x86-64/orca " +
                              inp_filename + " > " + output + "\n")
        else:
            bash_script.write("\n")
            bash_script.write("module load orca/4.2.0\n")
            bash_script.write("\n")
            bash_script.write("/system/software/generic/orca/orca_4_2_0_linux_x86-64/orca " +
                              inp_filename + " > " + output + "\n")
        bash_script.write("\n")
        if notrashtmp:
            bash_script.write("mkdir -p " + inp_filename.replace(".inp", "") + "_tmp" + "\n")
            bash_script.write("shopt -s extglob" + "\n")
            bash_script.write("mv !(*.inp|*.sh|*.out) " + inp_filename.replace(".inp", "") + "_tmp" + "\n")
            bash_script.write("shopt -u extglob" + "\n")
        if gbw:
            bash_script.write("shopt -s extglob" + "\n")
            bash_script.write("rm -f *.*.* && rm -f !(*.inp|*.out|*.sh|*.gbw)" + "\n")
            bash_script.write("shopt -u extglob" + "\n")
        else:
            bash_script.write("shopt -s extglob" + "\n")
            bash_script.write("rm -f *.*.* && rm -f !(*.inp|*.out|*.sh)" + "\n")
            bash_script.write("shopt -u extglob" + "\n")
        if noslurm:
            bash_script.write("rm -f slurm*.out" + "\n")
        else:
            pass
    return sh_filename


if __name__ == '__main__':

    args = get_args()

    for filename in args.filenames:
        script_filename = print_bash_script(filename, check_host(), args.notrashtmp, args.gbw, args.noslurm, args.np,
                                            args.nodes, args.t, args.devel, args.mem1T5, args.mem6T)
        subprocess.check_call(["sbatch", script_filename])

